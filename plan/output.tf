output "load-balancer-ip-address" {
  value = google_compute_global_forwarding_rule.webapp.ip_address
}
